import {AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {WorkspaceCanvasComponent} from '../../modules/design-tool/components/canvas/workspace-canvas.component';
import {DesignService} from '../../modules/design-tool/services/design.service';
import {ImageService} from '../../modules/design-tool/services/image.service';

@Component({
    templateUrl: './tool.component.html',
    styleUrls: ['./tool.component.less'],
    encapsulation: ViewEncapsulation.None
})

export class ToolComponent implements AfterViewInit {
    showFiller = false;
    public designData = {};
    public panelStatuses: any = {};

    @ViewChild('workspaceCanvas', {static: false}) workspaceCanvas: WorkspaceCanvasComponent;
    // @ViewChild('dragHandlers') dragHandlers: DragHandlersComponent;

    constructor(
        private designService: DesignService,
        private imageService: ImageService
    ) {}

    ngAfterViewInit(): void {
        const designData = this.designService.fetchDesignData(1);
        designData.elements = this.imageService.setDesignImages(designData.elements);
        this.updateDesignData(designData);
    }

    public updateDesignData(designData) {
        this.designData = designData;
        this.workspaceCanvas.setDesignData(this.designData);
    }

    // public editPanelStatus(statusObject) {
    //     if (statusObject.key) {
    //         this.panelStatuses[statusObject.key] = statusObject.value;
    //         this.canvas.updatePanelStatuses(this.panelStatuses);
    //         this.dragHandlers.updatePanelStatuses(this.panelStatuses);
    //     }
    // }
}
