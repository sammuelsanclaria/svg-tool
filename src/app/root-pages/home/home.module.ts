import {NgModule} from '@angular/core';
import {HomeComponent} from './home.component';
import {HomeRoutingModule} from './home-routing.module';

@NgModule({
    imports: [
        // routing modules
        HomeRoutingModule

        // component modules

        // service modules
    ],
    declarations: [
        HomeComponent
    ]
})

export class HomeModule {}
