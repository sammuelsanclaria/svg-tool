import {Component, ElementRef, Input, ViewChild} from '@angular/core';
import {ImageService} from '../../services/image.service';
import {TextService} from '../../services/text.service';

@Component({
    selector: 'workspace-canvas',
    templateUrl: './workspace-canvas.component.html'
})

export class WorkspaceCanvasComponent {
    @Input() selectedElementIndex = 0;
    @ViewChild('workspaceCanvas', {static: false}) workspaceCanvas: ElementRef;

    public panelStatuses: any = {};
    public designData: any = {};

    constructor(
        private imageService: ImageService,
        private textService: TextService
    ) {}

    public setDesignData(designData) {
        this.designData = designData;
        this.applyPageProperties(designData.page.sections);
        this.applyElements(designData.elements);
    }

    public updatePanelStatuses(panelStatuses) {
        this.panelStatuses = panelStatuses;
        this.applyPageProperties(this.designData.page.sections);
        this.applyElements(this.designData.elements);
    }

    private applyPageProperties(designSectionsData) {
        let width = 0;
        let height = 0;

        for (const pageSection of designSectionsData) {
            width += pageSection.size.width;
            height += pageSection.size.height;
        }

        this.workspaceCanvas.nativeElement.width = width;
        this.workspaceCanvas.nativeElement.height = height;
    }

    private applyElements(designElementsData) {
        for (let elementIndex = 0; elementIndex < designElementsData.length; elementIndex++) {
            const element = designElementsData[elementIndex];
            switch (element.type) {
                case 'IMAGE':
                    if (element.image) {
                        if (elementIndex === this.selectedElementIndex && this.panelStatuses.cropping) {
                            // uncomment to show placeholder
                            // this.imageService.drawCroppingOriginalImage(element, this.canvas.nativeElement);
                        }

                        this.imageService.drawImage(element, this.workspaceCanvas.nativeElement);
                    } else {
                        this.imageService.prepareImage(element).then((image) => {
                            element.image = image;
                            this.applyElements(designElementsData);
                        });
                    }

                    break;

                case 'TEXT':
                    this.textService.drawText(element, this.workspaceCanvas.nativeElement);
                    break;
            }
        }
    }
}

import {NgModule} from '@angular/core';

@NgModule({
    imports: [],
    declarations: [WorkspaceCanvasComponent],
    exports: [WorkspaceCanvasComponent]
})

export class CanvasModule {}
