import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class DesignService {
    public fetchDesignData(designId) {
        return {
            page: {
                sections: [
                    {
                        type: 'BACK_COVER',
                        name: 'Back Cover',
                        size: {
                            width: 800,
                            height: 500
                        }
                    }
                ]
            },
            elements: [
                {
                    type: 'IMAGE',
                    original_size: {
                        width: 475,
                        height: 320
                    },
                    stretched_size: {
                        width: 475,
                        height: 320
                    },
                    size: {
                        width: 475,
                        height: 320
                    },
                    coordinates: {
                        x: 100,
                        y: 100
                    },
                    cropping: {
                        coordinates: {
                            x: 0,
                            y: 0
                        },
                        size: {
                            width: 475,
                            height: 320
                        }
                    },
                    rotation: 0,
                    image_id: 1,
                    filters: {}
                }
            ]
        };
    }
}
