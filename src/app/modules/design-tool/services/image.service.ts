import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class ImageService {
    public setDesignImages(designData) {

        designData.forEach((value, index) => {
            if (value.type === 'IMAGE') {
                if (value.image_id === 1) {
                    designData[index].image_src = 'https://www.jeancoutu.com/globalassets/images-conseils/photo/' +
                        '20160414-02-astuces-photo-bebe/lead.jpg';

                } else if (value.image_id === 2) {
                    designData[index].image_src = 'https://www.jeancoutu.com/globalassets/images-conseils/photo/' +
                        '20160414-02-astuces-photo-bebe/seance_photo_bebe_ca_se_prepare.jpg';
                }
            }
        });

        return designData;
    }

    public prepareImage(imageData) {
        return new Promise((resolve, reject) => {
            const image = new Image();

            image.src = imageData.image_src;
            image.onload = () => {
                resolve(image);
            };
        });
    }

    public drawCroppingOriginalImage(imageData, canvas) {
        const canvasContext = canvas.getContext('2d');
        canvasContext.save();

        canvasContext.translate(
            imageData.coordinates.x + imageData.size.width / 2,
            imageData.coordinates.y + imageData.size.height  / 2
        );

        canvasContext.rotate(imageData.rotation * Math.PI / 180);
        canvasContext.drawImage(
            imageData.image,
            0, // cropping x coordinate
            0, // cropping y coordinate
            imageData.original_size.width, // cropping width
            imageData.original_size.height, // cropping height
            -(imageData.cropping.coordinates.x + imageData.size.width / 2), // image x coordinate (always centered)
            -(imageData.cropping.coordinates.y + imageData.size.height / 2), // image y coordinate (always centered)
            imageData.original_size.width, // image width
            imageData.original_size.height // image height
        );

        canvasContext.restore();
    }

    public drawImage(imageData, canvas) {
        const canvasContext = canvas.getContext('2d');
        canvasContext.save();

        canvasContext.translate(
            imageData.coordinates.x + imageData.size.width / 2,
            imageData.coordinates.y + imageData.size.height  / 2
        );

        canvasContext.rotate(imageData.rotation * Math.PI / 180);
        canvasContext.drawImage(
            imageData.image,
            imageData.cropping.coordinates.x, // cropping x coordinate
            imageData.cropping.coordinates.y, // cropping y coordinate
            imageData.cropping.size.width, // cropping width
            imageData.cropping.size.height, // cropping height
            -imageData.size.width / 2, // image x coordinate (always centered)
            -imageData.size.height / 2, // image y coordinate (always centered)
            imageData.size.width, // image width
            imageData.size.height // image height
        );

        canvasContext.restore();
    }
}
